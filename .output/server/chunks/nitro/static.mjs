import { createError } from 'h3';
import { withLeadingSlash, withoutTrailingSlash, parseURL } from 'ufo';
import { promises } from 'fs';
import { resolve, dirname } from 'pathe';
import { fileURLToPath } from 'url';

const assets = {
  "/_nuxt/bootstrap-44dc7190.mjs": {
    "type": "application/javascript",
    "etag": "\"12a00-yj7P6VunFheD+HdQWQQf0SzCXoQ\"",
    "mtime": "2022-03-18T19:55:17.268Z",
    "path": "../public/_nuxt/bootstrap-44dc7190.mjs"
  },
  "/_nuxt/entry-77fd1aa1.mjs": {
    "type": "application/javascript",
    "etag": "\"47-67KbWeAWCVzyqW7CVvui23EydhM\"",
    "mtime": "2022-03-18T19:55:17.268Z",
    "path": "../public/_nuxt/entry-77fd1aa1.mjs"
  },
  "/_nuxt/manifest.json": {
    "type": "application/json",
    "etag": "\"1d8-95O7FbHXYZ/ogG3cW2xAJU+Nve4\"",
    "mtime": "2022-03-18T19:55:17.268Z",
    "path": "../public/_nuxt/manifest.json"
  },
  "/_nuxt/assets/bootstrap.7dc97abe.css": {
    "type": "text/css; charset=utf-8",
    "etag": "\"11b-pRQapzFEO+87mwDycf2sYcLwOwI\"",
    "mtime": "2022-03-18T19:55:17.272Z",
    "path": "../public/_nuxt/assets/bootstrap.7dc97abe.css"
  }
};

const mainDir = dirname(fileURLToPath(globalThis.entryURL));

function readAsset (id) {
  return promises.readFile(resolve(mainDir, getAsset(id).path))
}

function getAsset (id) {
  return assets[id]
}

const METHODS = ["HEAD", "GET"];
const PUBLIC_PATH = "/_nuxt/";
const TWO_DAYS = 2 * 60 * 60 * 24;
const STATIC_ASSETS_BASE = "/home/lucas/Desktop/projetos/nuxtjs/dist" + "/" + "1647633315";
async function serveStatic(req, res) {
  if (!METHODS.includes(req.method)) {
    return;
  }
  let id = withLeadingSlash(withoutTrailingSlash(parseURL(req.url).pathname));
  let asset = getAsset(id);
  if (!asset) {
    const _id = id + "/index.html";
    const _asset = getAsset(_id);
    if (_asset) {
      asset = _asset;
      id = _id;
    }
  }
  if (!asset) {
    if (id.startsWith(PUBLIC_PATH) && !id.startsWith(STATIC_ASSETS_BASE)) {
      throw createError({
        statusMessage: "Cannot find static asset " + id,
        statusCode: 404
      });
    }
    return;
  }
  const ifNotMatch = req.headers["if-none-match"] === asset.etag;
  if (ifNotMatch) {
    res.statusCode = 304;
    return res.end("Not Modified (etag)");
  }
  const ifModifiedSinceH = req.headers["if-modified-since"];
  if (ifModifiedSinceH && asset.mtime) {
    if (new Date(ifModifiedSinceH) >= new Date(asset.mtime)) {
      res.statusCode = 304;
      return res.end("Not Modified (mtime)");
    }
  }
  if (asset.type) {
    res.setHeader("Content-Type", asset.type);
  }
  if (asset.etag) {
    res.setHeader("ETag", asset.etag);
  }
  if (asset.mtime) {
    res.setHeader("Last-Modified", asset.mtime);
  }
  if (id.startsWith(PUBLIC_PATH)) {
    res.setHeader("Cache-Control", `max-age=${TWO_DAYS}, immutable`);
  }
  const contents = await readAsset(id);
  return res.end(contents);
}

export { serveStatic as default };
