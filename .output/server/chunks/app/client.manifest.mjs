const client_manifest = {
  "node_modules/nuxt3/dist/app/entry.mjs": {
    "file": "entry-77fd1aa1.mjs",
    "src": "node_modules/nuxt3/dist/app/entry.mjs",
    "isEntry": true,
    "dynamicImports": [
      "node_modules/nuxt3/dist/app/bootstrap.mjs"
    ]
  },
  "node_modules/nuxt3/dist/app/bootstrap.mjs": {
    "file": "bootstrap-44dc7190.mjs",
    "src": "node_modules/nuxt3/dist/app/bootstrap.mjs",
    "isDynamicEntry": true,
    "css": [
      "assets/bootstrap.7dc97abe.css"
    ]
  }
};

export { client_manifest as default };
